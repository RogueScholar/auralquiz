# Auralquiz
# Copyright 2011-2021  JanKusanagi JRR <jancoding@gmx.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
#
# -------------------------------------------------
# Project created by QtCreator
# -------------------------------------------------


message("Generating Makefile for Auralquiz...")


QT += core gui widgets
message("Building with Qt v$$QT_VERSION")

qtHaveModule(phonon4qt5) {
    message("Phonon found OK")
    QT += phonon4qt5
} else {
    message("Phonon module phonon4qt5 not found, attempting to link directly...")
    LIBS += -lphonon4qt5
}


lessThan(QT_MAJOR_VERSION, 5) {
    warning(" >>> You're trying to build with Qt 4")
    warning(" >>> This version of Auralquiz requires Qt 5")
    warning(" >>> You might need to use qmake-qt5 instead $$escape_expand(\\n)")

    error("Aborting!")
}



TARGET = auralquiz
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050600



SOURCES += src/main.cpp \
    src/auralwindow.cpp \
    src/optionsdialog.cpp \
    src/answerbox.cpp \
    src/ranking.cpp \
    src/musicanalyzer.cpp

HEADERS += src/auralwindow.h \
    src/optionsdialog.h \
    src/answerbox.h \
    src/ranking.h \
    src/musicanalyzer.h


OTHER_FILES += README \
    LICENSE \
    CHANGELOG \
    org.nongnu.auralquiz.desktop \
    INSTALL \
    TODO \
    manual/auralquiz.6 \
    translations/translation-status \
    TRANSLATING


RESOURCES += auralquiz.qrc

TRANSLATIONS += translations/auralquiz_es.ts \
    translations/auralquiz_ca.ts \
    translations/auralquiz_gl.ts \
    translations/auralquiz_eu.ts \
    translations/auralquiz_cs.ts \
    translations/auralquiz_fr.ts \
    translations/auralquiz_it.ts \
    translations/auralquiz_de.ts \
    translations/auralquiz_pt.ts \
    translations/auralquiz_ru.ts \
    translations/auralquiz_pl.ts \
    translations/auralquiz_EMPTY.ts


LIBS += -ltag # to use TagLib


## This is here so the makefile has a 'make install' target
target.path = /usr/games/

desktop_file.files = org.nongnu.auralquiz.desktop
desktop_file.path = /usr/share/applications/

man_file.files = manual/auralquiz.6
man_file.path = /usr/share/man/man6/

appdata_file.files = appdata/org.nongnu.auralquiz.appdata.xml
appdata_file.path = /usr/share/metainfo/

#icon32_png.files = icon/32x32/auralquiz.png
#icon32_png.path = /usr/share/icons/hicolor/32x32/apps/

icon64_png.files = icon/64x64/auralquiz.png
icon64_png.path = /usr/share/icons/hicolor/64x64/apps/

INSTALLS += target \
            desktop_file \
            man_file \
            appdata_file \
            icon64_png


message("$$escape_expand(\\n\\n)")
message("Makefile done!")
message("If you're building the binary, you can run 'make' now.")
