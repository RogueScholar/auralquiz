/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2021  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "answerbox.h"


// Constructor
AnswerBox::AnswerBox(QWidget *parent)  :  QLineEdit(parent)
{
    this->setMinimumHeight(80);
    this->setAlignment(Qt::AlignCenter);

    const QString tooltipMessage = tr("Type the answer here");
    this->setPlaceholderText(tooltipMessage);
    this->setToolTip("<b></b>" + tooltipMessage);

    connect(this, &QLineEdit::returnPressed,
            this, &AnswerBox::validateAnswer);

    qDebug() << "AnswerBox created";
}


// Destructor
AnswerBox::~AnswerBox()
{
    qDebug() << "AnswerBox destroyed";
}


QString AnswerBox::normalizeString(QString original)
{
    // remove "(" and ")
    original = original.remove(QRegExp("\\(|\\)"));

    // remove "," and "."
    original = original.remove(QRegExp(",|\\."));


    // remove "-", "~", "*" and "/"
    original = original.remove(QRegExp("-|~|\\*|/"));

    // Turn "_" into " "
    original = original.replace(QRegExp("_"), QStringLiteral(" "));


    // Remove apostrophes or accents used like apostrophes
    original = original.remove(QRegExp(QString::fromUtf8("'|`|´")));


    // Make the string all lowercase
    original = original.toLower();


    // Turn accented vowels into normal ones.
    original = original.replace(QRegExp(QString::fromUtf8("à|á|ä|â")),  "a");
    original = original.replace(QRegExp(QString::fromUtf8("è|é|ë|ê")),  "e");
    original = original.replace(QRegExp(QString::fromUtf8("ì|í|ï|î")),  "i");
    original = original.replace(QRegExp(QString::fromUtf8("ò|ó|ö|ô")),  "o");
    original = original.replace(QRegExp(QString::fromUtf8("ù|ú|ü|û")),  "u");


    // Remove "!"
    original = original.remove(QRegExp("!"));

    // Remove "?"
    original = original.remove(QRegExp("\\?"));

    // Turn "&" into "and"
    //original = original.replace(QRegExp("&"), " and "); // FIXME: and non-english?

    // Turn possible double-spaces into one space
    original = original.replace(QRegExp("\\s+"), QStringLiteral(" "));

    // Trim spaces
    original = original.trimmed();

    return original;
}


void AnswerBox::setAnswer(QString newAnswer)
{
    m_correctAnswer = this->normalizeString(newAnswer);
    this->clear(); // Erase current text in the box, if any
}



void AnswerBox::validateAnswer()
{
    QString currentAnswer = this->text().trimmed();
    qDebug() << "Answered:" << currentAnswer;

    if (currentAnswer.isEmpty())  // User pressed Enter with no text!
    {
        return;
    }

    // Clean the entered string: any extra spaces, caps, &...
    currentAnswer = this->normalizeString(currentAnswer);

    qDebug() << "Answer after cleaning:" << currentAnswer;
    qDebug() << "Correct answer was:" << m_correctAnswer;


    if (currentAnswer == m_correctAnswer)
    {
        emit answered(true);
        //this->setStyleSheet("background: green");
    }
    else
    {
        emit answered(false);
        //this->setStyleSheet("background: red");
    }
}
