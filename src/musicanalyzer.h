/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2021  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MUSICANALYZER_H
#define MUSICANALYZER_H

#include <QObject>
#include <QFile>
#include <QDirIterator>
#include <QProgressDialog>
#include <QApplication>
#include <QMessageBox>
#include <QStringList>

#include <QDebug>

// GCC 4.7 requires this
#include <unistd.h>

// TagLib -> libtaglib-devel
#include <taglib/fileref.h>
#include <taglib/tag.h>
//#include <taglib/tstring.h>



class MusicAnalyzer : public QWidget
{
    Q_OBJECT

public:
    explicit MusicAnalyzer(QStringList musicDirectories, QString dataDirectory,
                           QStringList *musicFiles, QWidget *parent = nullptr);
    ~MusicAnalyzer();

    void setMusicDirectories(QStringList musicDirectories);

    void storeMetadataCache();

signals:
    void setStartGameButton(bool enabledState,
                            QString text,
                            QString tooltip);

public slots:
    void loadSongList();
    void createSongList();

private:
    QStringList m_musicDirectories;
    QString m_dataDirectory;


// tmp! ?
    QStringList *m_musicFiles; // Pointer to an ARRAY tmp FIXME

};

#endif // MUSICANALYZER_H
