/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2021  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QWidget>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QSlider>
#include <QCheckBox>
#include <QStringList>
#include <QLineEdit>
#include <QGroupBox>
#include <QFileDialog>
#include <QSettings>
#include <QDesktopServices>
#include <QShowEvent>
#include <QCloseEvent>
#include <QAction>
#include <QMessageBox>
#include <QListWidget>
#include <QFrame>

#include <QDebug>

class OptionsDialog : public QWidget
{
    Q_OBJECT

public:
    OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();

    bool isHomeInScanDirectories(QStringList directories);
    QStringList cleanupDirectoryList(QStringList directories);

    QStringList getMusicDirectories();

protected:
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);

public slots:
    void addMusicDirectory();
    void removeSelectedMusicDirectory();
    void setDifficultyEasier();
    void setDifficultyHarder();
    void updateDifficultyName(int newValue);
    void saveOptions();
    void saveAndReload();

    void showConfigMode();
    void showPlayMode();

signals:
    void optionsChanged(bool startGame, QStringList directories,
                        bool forceReload, int difficulty,
                        int questions, int players,
                        QStringList playerNameList, bool ownColors);

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *topLayout;
    QVBoxLayout *m_topRightLayout;
    QHBoxLayout *middleLayout;
    QVBoxLayout *difficultyLayout;
    QHBoxLayout *difficultyTopLayout;
    QGridLayout *rightSideLayout;
    QGridLayout *playerNamesLayout;
    QHBoxLayout *bottomLayout;

    QLabel *musicFromLabel;
    QLabel *currentMusicDirectoryLabel;
    QListWidget *m_directoriesListWidget;
    QPushButton *m_addDirectoryButton;
    QPushButton *m_removeDirectoryButton;
    QPushButton *reloadMusicInfoButton;

    QFrame *m_separatorFrame;

    QPushButton *difficultyEasy;
    QSlider *difficultyLevel;
    QPushButton *difficultyHard;
    QLabel *difficultyName;

    QLabel *numQuestionsLabel;
    QSpinBox *numQuestions;
    QLabel *numPlayersLabel;
    QSpinBox *numPlayers;

    QStringList playerNames;

    QGroupBox *playerNamesGroup;
    QLabel *player1Number;
    QLabel *player2Number;
    QLabel *player3Number;
    QLabel *player4Number;
    QLabel *player5Number;
    QLabel *player6Number;
    QLabel *player7Number;
    QLabel *player8Number;
    QLineEdit *player1Name;
    QLineEdit *player2Name;
    QLineEdit *player3Name;
    QLineEdit *player4Name;
    QLineEdit *player5Name;
    QLineEdit *player6Name;
    QLineEdit *player7Name;
    QLineEdit *player8Name;

    QCheckBox *useOwnColors;


    QPushButton *saveButton;
    QPushButton *cancelButton;

    QAction *startAction;
    QAction *closeAction;

    QStringList m_musicDirectories;
    bool reload;

    bool playMode;
};


#endif // OPTIONSDIALOG_H
