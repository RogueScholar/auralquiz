/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2021  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef RANKING_H
#define RANKING_H

#include <QWidget>
#include <QLabel>
#include <QTableWidget>
#include <QHeaderView>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QResizeEvent>
#include <QDebug>


class Ranking : public QWidget
{
    Q_OBJECT

public:
    Ranking(uint totalPlayers, QStringList playerNames,
            QList<int> playerScores, QList<int> playerGood,
            QList<int> playerBad, QList<int> playerTimedOut,
            QWidget *parent = 0);
    ~Ranking();

signals:
    void closed();

public slots:

protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void closeEvent(QCloseEvent *event);

private:
    QLabel *mainLabel;
    QTableWidget *rankingTable;
    QLabel *commentsLabel;
    QPushButton *closeButton;

    QVBoxLayout *mainLayout;
    QHBoxLayout *bottomLayout;


    int normalRowHeight;
};

#endif // RANKING_H
