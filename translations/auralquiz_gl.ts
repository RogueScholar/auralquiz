<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl">
<context>
    <name>AnswerBox</name>
    <message>
        <location filename="../src/answerbox.cpp" line="29"/>
        <source>Type the answer here</source>
        <translation>Escribe aquí a resposta</translation>
    </message>
</context>
<context>
    <name>AuralWindow</name>
    <message>
        <location filename="../src/auralwindow.cpp" line="312"/>
        <source>&amp;Start game</source>
        <translation>&amp;Comezar o xogo</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="328"/>
        <source>&amp;About...</source>
        <translation>&amp;Sobre...</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Saír</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="430"/>
        <source>Remaining time to answer this question</source>
        <translation>Tempo restante para responder á pregunta</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="434"/>
        <source>%v out of %m questions - %p%</source>
        <translation>%v de %m preguntas - %p%</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="437"/>
        <source>How many questions you&apos;ve answered</source>
        <translation>Cantas preguntas respondiches</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="439"/>
        <source>Score</source>
        <translation>Puntuación</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="446"/>
        <source>Your current score</source>
        <translation>A túa puntuación actual</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="454"/>
        <source>&amp;End game</source>
        <translation>&amp;Fin do xogo</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="482"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="664"/>
        <source>Sound system error</source>
        <translation>Produciuse un erro no sistema de son</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="665"/>
        <source>There seems to be a problem with your sound system.</source>
        <translation>Semella que hai un problema con teu sistema de son.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="668"/>
        <source>Maybe you don&apos;t have any Phonon backends installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="790"/>
        <source>%1 songs available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1122"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. Feel free to add your contact information, such as e-mail. If there was another translator before you, add your name after theirs ;)</comment>
        <translation type="unfinished">Traducido ao galego por Miguel Anxo Bouzada.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="43"/>
        <source>This seems to be the first time you use Auralquiz.
Before playing, your music will be analyzed.
If needed, you should click the Options button and select the folder where your Ogg, FLAC and MP3 files are stored.

This folder, and sub-folders will be scanned so Auralquiz can generate questions and answers about your music.

You need files correctly tagged in order for the game to work correctly.

The scan can take some time, and the program will not be responsive until it is complete. Please be patient.</source>
        <translation>Semella que esta é a primeira vez que usas Auralquiz.
Antes de xogar, analizarase a túa música.
Se for necesario, deberás premer no botón «Opcións» e seleccionar o cartafol onde se atopan os teus ficheiros Ogg, FLAC e MP3.

Este cartafol e os seus subcartafoles van seren analizados para que Auralquiz poida xerar preguntas e respostas sobre a túa música.

Necesitas ficheiros etiquetados correctamente para que o xogo funcione ben.

A análise pode levar bastante tempo e o programa non responderá ata que estea completo. Ten paciencia mentres.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="944"/>
        <source>Error playing sound</source>
        <translation>Produciuse un erro ao reproducir o son</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="945"/>
        <source>An error occurred while playing sound.
The error message was: %1

Maybe your Phonon-backend does not have support for the MP3 file format.</source>
        <translation>Produciuse un erro ao reproducir o son.
A mensaxe de erro foi: %1

É probábel que a infraestrutura Phonon non teña compatibilidade cos formato de ficheiro MP3.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1030"/>
        <source>Time&apos;s up!!</source>
        <translation>Rematou o tempo!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1106"/>
        <source>About Auralquiz</source>
        <translation>Sobre o Auralquiz</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1117"/>
        <source>Auralquiz loads all your music from a specified folder and asks questions about it, playing a short piece of each music file as clue.</source>
        <translation>Auralquiz carga toda a música dun cartafol indicado e fai preguntas sobre ela, reproducindo un cacho pequeno de cada ficheiro de música como pista.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="777"/>
        <source>Starting!</source>
        <translation>Comezamos!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="264"/>
        <source>Good</source>
        <translation>Ben</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="273"/>
        <source>Bad</source>
        <translation>Mal</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="282"/>
        <source>Timed out</source>
        <translation>Tiempo agotado</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="778"/>
        <source>Let&apos;s go!</source>
        <translation>Imos aló!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="779"/>
        <source>GO!!</source>
        <translation>Imos!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="780"/>
        <source>Good luck!</source>
        <translation>Boa sorte!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1031"/>
        <source>The answer was:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1129"/>
        <source>Thanks to all the testers, translators and packagers, who help make Auralquiz better!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1133"/>
        <source>Auralquiz is licensed under the GNU GPL license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1135"/>
        <source>Main screen image and some icons are based on Oxygen icons, under LGPL license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1208"/>
        <source>Player %1 of %2</source>
        <translation>Xogador %1 de %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1373"/>
        <source>Go, %1!</source>
        <comment>%1=player&apos;s name</comment>
        <translation type="unfinished">Imos, %1!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1470"/>
        <source>%1 by %2</source>
        <comment>1=song title, 2=author name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1475"/>
        <source>+%1 points!</source>
        <translation>+%1 puntos!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1501"/>
        <source>Correct answer was:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1572"/>
        <source>End game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1573"/>
        <source>Are you sure you want to end this game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;Yes, end it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="42"/>
        <source>First usage</source>
        <translation>Primeiro uso</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1319"/>
        <source>Not enough music files</source>
        <translation>Non hai ficheiros de música abondo</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1320"/>
        <source>You don&apos;t have enough music files, or from enough different artists.
You need music from at least 5 different artists to be able to generate questions.</source>
        <translation>Non tes ficheiros de música abondo, ou de máis artistas diferentes.
Necesitas música de polo menos 5 artistas diferentes para poder xerar preguntas.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1379"/>
        <source>Next!</source>
        <translation>Seguinte!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1454"/>
        <source>Correct!!</source>
        <translation>Correcto!!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1455"/>
        <source>Yeah!</source>
        <translation>Si!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1456"/>
        <source>That&apos;s right!</source>
        <translation>Iso é!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1457"/>
        <source>Good!</source>
        <translation>Ben!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1458"/>
        <source>Great!</source>
        <translation>Xenial!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1486"/>
        <source>Wrong!</source>
        <translation>Trabucácheste!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1487"/>
        <source>No!</source>
        <translation>Non!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1489"/>
        <source>Ooops!</source>
        <translation type="unfinished">Bótalle terra!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1490"/>
        <source>FAIL!</source>
        <translation>Fallaches!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1488"/>
        <source>That&apos;s not it.</source>
        <translation>Non, esa non.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1408"/>
        <source>All done!</source>
        <translation>Feito!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="321"/>
        <source>&amp;Options</source>
        <translation>&amp;Opcións</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="427"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1198"/>
        <source>Who plays this song?</source>
        <translation>Quen interpreta esta canción?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1202"/>
        <source>What&apos;s the title of this song?</source>
        <translation>Cal é o título desta canción?</translation>
    </message>
</context>
<context>
    <name>MusicAnalyzer</name>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="125"/>
        <source>Configure your music folder</source>
        <translation>Configura o teu cartafol da música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="214"/>
        <source>Analyzing music. Please wait</source>
        <translation>Analizando a música. Agarda un chisco</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="223"/>
        <source>Getting list of files under
%1</source>
        <comment>%1 is a folder</comment>
        <translation>Obtendo a lista de ficheiros en
%1</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="230"/>
        <source>Listing files...</source>
        <translation>Listando os ficheiros...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="255"/>
        <location filename="../src/musicanalyzer.cpp" line="361"/>
        <source>Please reload music info</source>
        <translation>Volve a cargar a información da música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="256"/>
        <source>Music analysis was cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="269"/>
        <source>There are no valid music files in your selected music directory.</source>
        <translation type="unfinished">Non hai ficheiros de música que poder empregar no cartafol de música seleccionado.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="272"/>
        <source>Please, select another directory containing Ogg, FLAC or MP3 files, and try again.</source>
        <translation type="unfinished">Escolle outro cartafol que conteña ficheiros Ogg, FLAC ou MP3, e téntao de novo.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="276"/>
        <source>There is no music!</source>
        <translation>Non hai música!</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="280"/>
        <location filename="../src/musicanalyzer.cpp" line="385"/>
        <source>Please choose another music folder</source>
        <translation>Escolle outro cartafol de música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="292"/>
        <source>Analyzing %1 files
under %2</source>
        <comment>%1 is a number, %2 is a folder</comment>
        <translation>Analizando %1 ficheiros en %2</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="297"/>
        <source>Cancel analysis</source>
        <translation>Cancelar a análise</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="300"/>
        <source>Analyzing your music...</source>
        <translation>Analizando a túa música...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="386"/>
        <source>The folder you selected doesn&apos;t have enough valid music files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="396"/>
        <source>&amp;Start game</source>
        <translation>&amp;Comezar o xogo</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/optionsdialog.cpp" line="31"/>
        <source>Game options</source>
        <translation>Opcións do xogo</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="59"/>
        <source>Using music from:</source>
        <translation>Usando música de:</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="64"/>
        <source>Select your &amp;music folder...</source>
        <translation>Selecciona o teu cartafol da &amp;música...</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="83"/>
        <source>&amp;Easier</source>
        <translatorcomment>&amp;Doado</translatorcomment>
        <translation>&amp;Facil</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="98"/>
        <source>&amp;Harder</source>
        <translatorcomment>Di&amp;fícil</translatorcomment>
        <translation>&amp;Difícil</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="126"/>
        <source>How many &amp;questions?</source>
        <translation>Cantas &amp;preguntas?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="131"/>
        <source>questions</source>
        <translation>preguntas</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="135"/>
        <source>How many &amp;players?</source>
        <translation>Cantos &amp;xogadores?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="140"/>
        <source>players</source>
        <translation>xogadores</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="144"/>
        <source>Use own color theme</source>
        <translation>Usar o tema de cores propio</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="209"/>
        <source>Player names</source>
        <translation>Nomes dos xogadores</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="225"/>
        <source>Save and &amp;reload music</source>
        <translation>Gardar e &amp;recargar a música</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="363"/>
        <source>In this level you&apos;ll have to type the answer.</source>
        <translation type="unfinished">Neste nivel terás que escribir a resposta.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="366"/>
        <source>It&apos;s not too strict, so you don&apos;t have to worry about caps, commas and such.</source>
        <translation type="unfinished">Non é moi estrito, así que non terás que preocuparte das maiúsculas, comas, etc.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="373"/>
        <source>In this level you&apos;ll have to click the correct button, or press the 1, 2, 3 and 4 keys in your keyboard.</source>
        <translation type="unfinished">Neste nivel terás que premer no botón correcto, ou premer nas nas teclas 1, 2, 3 e 4 do teclado.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="460"/>
        <source>&amp;Save options</source>
        <translation>&amp;Gardar as opcións</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="234"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="304"/>
        <source>Select where your music is located</source>
        <translation>Selecciona onde se atopa a túa música</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="351"/>
        <source>Piece of cake</source>
        <translation>Está chupado</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="352"/>
        <source>Let&apos;s rock</source>
        <translation>Un pouco de lio</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="353"/>
        <source>Come get some</source>
        <translation>Isto atravesase un chisco</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="354"/>
        <source>Damn I&apos;m good</source>
        <translation>Sonche moi bo</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="355"/>
        <source>HARD!!</source>
        <translation>Moi difícil!!</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="356"/>
        <source>Hardcore! Type the answer</source>
        <translation>Moi difícil! Escribe a resposta</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="490"/>
        <source>Start!</source>
        <translation>Comezar!</translation>
    </message>
</context>
<context>
    <name>Ranking</name>
    <message>
        <location filename="../src/ranking.cpp" line="27"/>
        <source>Game ended</source>
        <translation>Xogo rematado</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="33"/>
        <source>All questions done!</source>
        <translation>Contestaches todas as preguntas!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="35"/>
        <source>Thanks for playing!</source>
        <translation>Grazas por xogar!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="36"/>
        <source>Ranking</source>
        <translation>Clasificación</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="44"/>
        <source>Player</source>
        <translation>Xogador</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="45"/>
        <source>Score</source>
        <translation>Puntuación</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="46"/>
        <source>Good</source>
        <translation>Ben</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="47"/>
        <source>Bad</source>
        <translation>Mal</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="48"/>
        <source>T.O.</source>
        <comment>Timed out abbreviation</comment>
        <translation>T.E.</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="124"/>
        <source>What?? Zero points!!
You&apos;re all sooo bad!</source>
        <comment>Plural, all players</comment>
        <translation>Queee!! Cero puntos??
Sodes todos moooi malos!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="130"/>
        <source>Zero points!
%1, you&apos;re very bad!!</source>
        <comment>%1 is the name of a player</comment>
        <translation>Cero puntos!
%1, es moi malo!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="146"/>
        <source>Draw between %1 and %2!</source>
        <comment>%1 and %2 are player names</comment>
        <translation>Empate entre %1 y %2!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="156"/>
        <source>You&apos;re very good!!</source>
        <translation>Es moi bo!!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="163"/>
        <source>Congratulations, %1!!</source>
        <comment>%1 is the name of the winner</comment>
        <translation>Parabéns, %1!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="173"/>
        <source>That was amazing!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="177"/>
        <source>That was quite good!</source>
        <translation>Estiveches bastante ben!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="181"/>
        <source>You can do better...</source>
        <translation>Podes facelo mellor...</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="160"/>
        <source>You&apos;re good!</source>
        <translation>Es bo!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="197"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
</context>
</TS>
